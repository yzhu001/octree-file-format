#! /bin/sh

module load GCCcore/11.2.0
module load binutils/2.37-GCCcore-11.2.0
module load CUDA/11.4.1
module load CMake/3.22.1-GCCcore-11.2.0
module load HDF5/1.12.1-gompi-2021b

OCTREE_ROOT=$PWD
cd $OCTREE_ROOT/liboctree
mkdir build && cmake -S . -B build
cd build && make
cd $OCTREE_ROOT/octreegen/
mkdir build
cmake \
    -DOCTREE_INCLUDE_DIR=$OCTREE_ROOT/liboctree/include \
    -DOCTREE_LIBRARIES=$OCTREE_ROOT/liboctree/build/liboctree.so \
    -DCMAKE_C_COMPILER=gcc \
    -DCMAKE_CXX_COMPILER=g++ \
    -S . -B build
cd build && make